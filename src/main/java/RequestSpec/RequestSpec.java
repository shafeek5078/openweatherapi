package RequestSpec;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class RequestSpec extends configs{

    public static RequestSpecification getWeatherReqSpec() {
        return new RequestSpecBuilder().setBaseUri(configs.baseUri)
                .setContentType(ContentType.JSON)
                .build();
    }
}