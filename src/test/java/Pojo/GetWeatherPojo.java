package Pojo;

import java.util.List;

public class GetWeatherPojo {
    private coord coord;
    private String base;
    private mainpojo main;
    private String visibility;
    private wind wind;

    public Pojo.clouds getClouds() {
        return clouds;
    }

    public void setClouds(Pojo.clouds clouds) {
        this.clouds = clouds;
    }

    private clouds clouds;
    private long dt;
    private sys sys;
    private int timezone;
    private int id;
    private List<weather> weather;


    public Pojo.wind getWind() {
        return wind;
    }

    public void setWind(Pojo.wind wind) {
        this.wind = wind;
    }

    public List<Pojo.weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Pojo.weather> weather) {
        this.weather = weather;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;
    private int cod;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public mainpojo getMain() {
        return main;
    }

    public void setMain(mainpojo main) {
        this.main = main;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public Pojo.sys getSys() {
        return sys;
    }

    public void setSys(Pojo.sys sys) {
        this.sys = sys;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pojo.coord getCoord() {
        return coord;
    }

    public void setCoord(Pojo.coord coord) {
        this.coord = coord;
    }
}