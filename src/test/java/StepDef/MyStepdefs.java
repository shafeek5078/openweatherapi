package StepDef;

import Pojo.GetWeatherPojo;
import RequestSpec.RequestSpec;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

import static ResponseSpec.ResponseSpec.resSpec;
import static io.restassured.RestAssured.given;

public class MyStepdefs{
    private RequestSpecification res;
    String responseString;
    int temp;
    String reponseCity;

    @Given("I have Created Get weather Payload")
    public void iHaveCreatedGetWeatherPayload() {
        res = given().spec(RequestSpec.getWeatherReqSpec());
    }

    @When("I triggred getWeather API call and get the response for the city {string}")
    public void iTriggredGetWeatherAPICallAndGetTheResponseForTheCity(String city) {
        String apikey = RequestSpec.apiKey;
        Response responsedata = res.expect()
                .when().get("/data/2.5/weather?q="+city+"&units=metric&appid="+apikey)
                .then()
                .spec(resSpec())
                .extract().response();

         GetWeatherPojo gw= responsedata.as(GetWeatherPojo.class);
        responseString = responsedata.asString();
        System.out.println(responseString);

        System.out.println("Temp found from pojo :) -- >"+ gw.getMain().getTemp());
    }

    @Then("The Response body should cointains the temperatue of the city {string}")
    public void theResponseBodyShouldCointainsTheTemperatueOfTheCity(String arg0) {

        temp = Integer.parseInt(responseString.split("temp\":")[1].split(",")[0]);
        System.out.println("Temp in celcius --->" + temp);
        reponseCity = responseString.split("name\":")[1].split(",")[0];
    }

    @And("The dress to be packed for the trip should be {string}")
    public void theDressToBePackedForTheTripShouldBe(String dress) {
        if (temp <= 20)
            Assert.assertEquals(dress, "Wollens");
        else{
            System.out.println("Temperature is hot");
            Assert.assertEquals(dress, "Swimmers");
        }
    }
}