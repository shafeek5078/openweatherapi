Feature: Validate the weather condition in london city.
#Positive flow
  Scenario: Verify the temp to pack Swimmers for the trip
    Given I have Created Get weather Payload
    When I triggred getWeather API call and get the response for the city "Chennai"
    Then The Response body should cointains the temperatue of the city "Chennai"
    And The dress to be packed for the trip should be "Swimmers"

    #Negative Flow
  Scenario: Verify the temp to pack Wollens for the trip
    Given I have Created Get weather Payload
    When I triggred getWeather API call and get the response for the city "Chennai"
    Then The Response body should cointains the temperatue of the city "Chennai"
    And The dress to be packed for the trip should be "Wollens"